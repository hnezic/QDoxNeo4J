name := "GDoxNeo4J"

version := "0.1"

scalaVersion := "2.12.6"

libraryDependencies += "com.thoughtworks.qdox" % "qdox" % "1.12.1"

libraryDependencies += "org.neo4j" % "neo4j-ogm-core" % "3.1.2"

libraryDependencies += "org.neo4j" % "neo4j-ogm-bolt-driver" % "3.1.2"

libraryDependencies += "org.apache.commons" % "commons-io" % "1.3.2"
