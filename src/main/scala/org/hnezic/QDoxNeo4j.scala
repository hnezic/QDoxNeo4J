package org.hnezic

import java.io.File

import com.thoughtworks.qdox.JavaDocBuilder
import org.neo4j.ogm.config.Configuration
import org.neo4j.ogm.session.SessionFactory

import org.hnezic.models.JPackageList

object QDoxNeo4j extends App
{
    // Read arguments
    val path = args(0)
    val packagePrefix = args(1)
    val uri = args(2)

    // Create model
    val model = Model (path, packagePrefix)

    // Save model
    val configuration = new Configuration.Builder().uri(uri).build
    val sessionFactory = new SessionFactory(configuration, "org.hnezic.models")
    val session = sessionFactory.openSession()

    model.saveToNeo (session)

    sessionFactory.close()
}
