package org.hnezic.models

import com.thoughtworks.qdox.model.{JavaClass, JavaPackage}
import org.neo4j.ogm.annotation.{GeneratedValue, Id, NodeEntity, Relationship}

import scala.beans.BeanProperty

@NodeEntity(label="Package")
class JPackage (javaPackage: JavaPackage, packagePrefix: String) extends JNode
{
    @Id @GeneratedValue
    @BeanProperty
    var id: java.lang.Long = null

    @BeanProperty
    var shortName: String = javaPackage.getName.stripPrefix(packagePrefix)

    @BeanProperty
    var fullName: String = javaPackage.getName

    @Relationship("HAS_CLASS")
    @BeanProperty
    var classes: Array[JClass] = javaPackage.getClasses . filter (JPackage.isClass(_)) . map (javaClass => new JClass(javaClass))

    @Relationship("HAS_INTERFACE")
    @BeanProperty
    var interfaces: Array[JInterface] = javaPackage.getClasses . filter (_.isInterface) . map (javaClass => new JInterface(javaClass))
}

object JPackage
{
    def isClass (cl: JavaClass) : Boolean =
    {
        ! cl.isInterface && ! cl.isEnum && ! cl.isInner
    }
}