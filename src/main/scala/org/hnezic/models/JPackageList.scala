package org.hnezic.models

import com.thoughtworks.qdox.model.JavaPackage
import org.neo4j.ogm.annotation.{GeneratedValue, Id, NodeEntity, Relationship}

import scala.beans.BeanProperty

@NodeEntity(label="PackageList")
class JPackageList (_name: String, javaArray: Array[JavaPackage], packagePrefix: String) extends JNode
{
    @Id @GeneratedValue
    @BeanProperty
    var id: java.lang.Long = null

    @BeanProperty
    var name = _name

    @Relationship("HAS_PACKAGE")
    @BeanProperty
    var packages: Array[JPackage] = javaArray map (javaPackage => new JPackage(javaPackage, packagePrefix))
}

@NodeEntity(label="MavenModule")
class JMavenModule (nameArg: String, javaArray: Array[JavaPackage], packagePrefix: String) extends JPackageList (nameArg, javaArray, packagePrefix)
