package org.hnezic.models

import com.thoughtworks.qdox.model.JavaMethod
import org.neo4j.ogm.annotation.{GeneratedValue, Id, NodeEntity}

import scala.beans.BeanProperty

@NodeEntity(label="Method")
class JMethod (javaMethod: JavaMethod) extends JNode
{
    @Id @GeneratedValue
    @BeanProperty
    var id: java.lang.Long = null

    @BeanProperty
    var name: String = javaMethod.getName

}
