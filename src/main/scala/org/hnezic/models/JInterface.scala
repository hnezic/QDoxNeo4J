package org.hnezic.models

import com.thoughtworks.qdox.model.JavaClass
import org.neo4j.ogm.annotation.{GeneratedValue, Id, NodeEntity}

import scala.beans.BeanProperty

@NodeEntity(label="Interface")
class JInterface (javaClass: JavaClass) extends JNode
{
    @Id @GeneratedValue
    @BeanProperty
    var id: java.lang.Long = null

    @BeanProperty
    var name: String = javaClass.getName
}
