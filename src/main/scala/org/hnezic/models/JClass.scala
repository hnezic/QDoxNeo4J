package org.hnezic.models

import com.thoughtworks.qdox.model.JavaClass
import org.neo4j.ogm.annotation.{GeneratedValue, Id, NodeEntity, Relationship}

import scala.beans.BeanProperty

@NodeEntity(label="Class")
class JClass (javaClass: JavaClass) extends JNode
{
    @Id @GeneratedValue
    @BeanProperty
    var id: java.lang.Long = null

    @BeanProperty
    var name: String = javaClass.getName

    @Relationship("HAS_INNER_CLASS")
    @BeanProperty
    var innerClasses: Array[JClass] = javaClass.getNestedClasses . filter (JClass.isNestedClass(_)) . map (javaClass => new JClass(javaClass))

    @Relationship("HAS_METHOD")
    @BeanProperty
    var methods: Array[JMethod] = javaClass.getMethods . map (javaMethod => new JMethod(javaMethod))
}

object JClass
{
    def isNestedClass (cl: JavaClass) : Boolean =
    {
        ! cl.isInterface && ! cl.isEnum
    }
}
