package org.hnezic.models

import org.neo4j.ogm.annotation.{GeneratedValue, Id, NodeEntity, Relationship}

import scala.beans.BeanProperty

@NodeEntity(label="MavenModuleList")
class JMavenModuleList (_mavenModules: Array[JMavenModule]) extends JNode
{
    @Id @GeneratedValue
    @BeanProperty
    var id: java.lang.Long = null

    @BeanProperty
    var name = "Modules"

    @Relationship("HAS_MODULE")
    @BeanProperty
    var mavenModules: Array[JMavenModule] = _mavenModules
}
