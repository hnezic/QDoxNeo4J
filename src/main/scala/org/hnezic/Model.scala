package org.hnezic

import java.io.File

import com.thoughtworks.qdox.JavaDocBuilder
import org.apache.commons.io.FilenameUtils
import org.hnezic.models.{JMavenModule, JMavenModuleList, JNode, JPackageList}
import org.neo4j.ogm.session.Session

class Model (rootNode: JNode)
{
    def saveToNeo (session: Session) : Unit =
    {
        session.save (rootNode, 1000)
    }
}

object Model
{
    def apply (path: String, packagePrefix: String) : Model =
    {
        // Check if there are Maven modules
        val moduleDirs = getMavenModuleDirs (path)
        if (moduleDirs.isEmpty)
        {
            // A single package list
            val packageList = createPackageList (path, packagePrefix)
            new Model (packageList)
        }
        else
        {
            // Multiple Maven modules
            // val packageList = createPackageList (path, packagePrefix)
            // new Model (packageList)
            val mavenModules: Array[JMavenModule] = (moduleDirs map (dir => createMavenModule (dir.getPath, packagePrefix))).toArray
            val modulesList = new JMavenModuleList (mavenModules)
            new Model (modulesList)
        }
    }

    private def createPackageList (path: String, packagePrefix: String) : JPackageList =
    {
        val sourceFolder = new File(path)
        val builder = new JavaDocBuilder
        builder.addSourceTree(sourceFolder)

        new JPackageList ("Packages", builder.getPackages, packagePrefix)
    }

    private def createMavenModule (path: String, packagePrefix: String) : JMavenModule =
    {
        def getModuleName (folder: File) : String =
        {
            FilenameUtils.getName (folder.getName)
        }

        val sourceFolder = new File(path)
        val builder = new JavaDocBuilder
        builder.addSourceTree(sourceFolder)

        new JMavenModule (getModuleName(sourceFolder), builder.getPackages.filter (_ != null), packagePrefix)
    }

    private def getMavenModuleDirs (path: String) : List[File] =
    {
        def getListOfDirs : List[File] =
        {
            val dir = new File(path)
            if (dir.exists && dir.isDirectory) {
                dir.listFiles.filter(_.isDirectory).toList
            } else {
                List[File]()
            }
        }

        def fileIsPomXml (file: File) : Boolean =
        {
            val fileName = file.getName
            FilenameUtils.getName(fileName).equalsIgnoreCase("pom.xml")
        }

        def dirContainsPomXml (dir: File) : Boolean =
        {
            dir.listFiles.filter(fileIsPomXml(_)).toList.nonEmpty
        }

        getListOfDirs.filter(dirContainsPomXml(_))
    }
}
